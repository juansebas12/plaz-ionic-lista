import { Component, OnInit } from '@angular/core';
import { DataService } from './../../servicios/data.service';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-transportadores',
  templateUrl: './transportadores.page.html',
  styleUrls: ['./transportadores.page.scss'],
})
export class TransportadoresPage implements OnInit {

  tipPer: string;
  strDefHref: string;
  pedInfoTrans: any;
  colorEst: string;
  txtEst: string;
  habilitado = false;
  constructor(private rutaP: ActivatedRoute,
              private dataService: DataService,
              private navCtrl: NavController) { }

  ngOnInit() {
    this.tipPer = this.rutaP.snapshot.paramMap.get('tipP');
    if (this.tipPer === 'OPER') {
      this.strDefHref =  'operaciones';
      this.habilitado = true;
    }
    this.dataService.getPedTransportadores()
    .subscribe((data) => {
      this.pedInfoTrans = data;
    });
  }

  onClickPed(idPed: string) {
    this.navCtrl.navigateRoot('infopedtran/' + idPed + '/' + this.tipPer);
  }

}
