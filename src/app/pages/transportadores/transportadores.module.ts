import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransportadoresPageRoutingModule } from './transportadores-routing.module';

import { TransportadoresPage } from './transportadores.page';
import { ComponentesModule } from '../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransportadoresPageRoutingModule,
    ComponentesModule
  ],
  declarations: [TransportadoresPage]
})
export class TransportadoresPageModule {}
