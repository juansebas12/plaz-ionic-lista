import { DespachoService } from './../../servicios/api/despacho.service';
import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-despacho',
  templateUrl: './despacho.page.html',
  styleUrls: ['./despacho.page.scss'],
})
export class DespachoPage implements OnInit {
  nvlPriv: string;
  pedInfoProv: any;
  user : null;
  constructor(private rutaP: ActivatedRoute,
              private dataService: DespachoService,
              private navCtrl: NavController) { }

  async ngOnInit() {
    this.user = JSON.parse(await localStorage.getItem('user'));
    this.dataService.getPedidos(this.nvlPriv)
      .subscribe((data) => {
        console.log(data);
        this.pedInfoProv = data;
      });
  }
  async cambiarEstado(id,estado){
    this.dataService.cambiarEstado(id,estado).subscribe((data)=>{
      this.pedInfoProv = data;
    })
  }
}
