import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfopedtranPage } from './infopedtran.page';

const routes: Routes = [
  {
    path: '',
    component: InfopedtranPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfopedtranPageRoutingModule {}
