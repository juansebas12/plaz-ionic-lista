import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-infopedprov',
  templateUrl: './infopedprov.page.html',
  styleUrls: ['./infopedprov.page.scss'],
})
export class InfopedprovPage implements OnInit {

  idPed: string;
  nvPrv: string;
  tipP: string;
  strDefHref: string;
  constructor(private rutaP: ActivatedRoute) { }

  ngOnInit() {
    this.idPed = this.rutaP.snapshot.paramMap.get('idPed');
    this.nvPrv = this.rutaP.snapshot.paramMap.get('nvPriv');
    this.tipP = this.rutaP.snapshot.paramMap.get('tipP');
    if (this.tipP === 'OPER') {
      this.strDefHref = 'OPER';
    } else {
      this.strDefHref = 'PROV';
    }
  }
}
