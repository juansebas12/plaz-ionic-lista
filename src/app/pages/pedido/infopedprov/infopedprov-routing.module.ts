import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InfopedprovPage } from './infopedprov.page';

const routes: Routes = [
  {
    path: '',
    component: InfopedprovPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InfopedprovPageRoutingModule {}
