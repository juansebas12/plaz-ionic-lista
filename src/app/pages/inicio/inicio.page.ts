import { Component, OnInit } from '@angular/core';
import { DataService } from './../../servicios/data.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  constructor(private dataService: DataService,private navCtrl: NavController) { }
  infoUsuario: any;
  ngOnInit() {
    if(localStorage.getItem('id')){
      this.dataService.getUsuarioLogin()
      .subscribe(data => {
        console.log(data)
        this.infoUsuario = data;
          this.redirigeUsuario(data['tipo_persona'], data['usuario']);
        
      });
    }else{
      console.log('no existe');
    }
  }
  redirigeUsuario(tipUsu: string, priv: string) {
    switch (tipUsu) {
      case 'PROV':
        this.navCtrl.navigateRoot('/proveedores/'+priv);
        break;
      case 'ALIS':
        this.navCtrl.navigateRoot('/proveedor/' + priv + '/PROV');
        break;
      case 'DESP':
        this.navCtrl.navigateRoot('/despacho/'+priv);
        break;
    }
  }
}
