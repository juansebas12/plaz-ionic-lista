import { DataService } from './../../servicios/data.service';
import { Component, OnInit, Input } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-proveedor',
  templateUrl: './proveedor.page.html',
  styleUrls: ['./proveedor.page.scss'],
})
export class ProveedorPage implements OnInit {

  nvlPriv: string;
  tipP: string;
  pedInfoProv: any;
  user : null;
  habilitado = false;
  strDefHref: string;

  constructor(private rutaP: ActivatedRoute,
              private dataService: DataService,
              private navCtrl: NavController) { }

  async ngOnInit() {
    this.user = JSON.parse(await localStorage.getItem('user'));
    this.nvlPriv = this.rutaP.snapshot.paramMap.get('nvPrv');
    this.tipP = this.rutaP.snapshot.paramMap.get('tipP');
    if (this.tipP === 'OPER') {
      this.habilitado = true;
      this.strDefHref = 'operaciones-prov';
    }
    this.dataService.getPedProveedor(this.nvlPriv)
      .subscribe((data) => {
        console.log(data);
        this.pedInfoProv = data;
      });
  }
  cambiar(){
    localStorage.setItem('id',"");
    localStorage.setItem('user',"");
    this.navCtrl.navigateRoot('inicio');
  }
  onClickPed(idPed: string) {
    this.navCtrl.navigateRoot('infopedprov/' + idPed + '/' + this.nvlPriv + '/' + this.tipP);
  }

}
