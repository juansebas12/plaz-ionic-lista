import { Component, OnInit } from '@angular/core';
import { ProveedorService } from './../../servicios/api/proveedor.service';
@Component({
  selector: 'app-usuario-proveedor',
  templateUrl: './usuario-proveedor.page.html',
  styleUrls: ['./usuario-proveedor.page.scss'],
})
export class UsuarioProveedorPage implements OnInit {

  proveedores: any;
  productos : any;
  usuario : any;
  constructor(private service: ProveedorService) { }
  ngOnInit() {
    this.service.getProveedores()
      .subscribe((data) => {
        this.proveedores = data;
        console.log(this.proveedores);
      });
  }
  cambiousuario() {
    this.service.getProveedorProductos(this.usuario)
      .subscribe((data) => {
        this.productos = data;
        console.log(this.productos);
      });
  }
  cambiousuarioData(data){
    this.cambiousuario();
  }
}
