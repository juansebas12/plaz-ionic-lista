import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsuarioProveedorPage } from './usuario-proveedor.page';

const routes: Routes = [
  {
    path: '',
    component: UsuarioProveedorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsuarioProveedorPageRoutingModule {}
