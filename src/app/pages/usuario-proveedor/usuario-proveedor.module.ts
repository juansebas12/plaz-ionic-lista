import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsuarioProveedorPageRoutingModule } from './usuario-proveedor-routing.module';

import { UsuarioProveedorPage } from './usuario-proveedor.page';
import { ComponentesModule } from '../../componentes/componentes.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsuarioProveedorPageRoutingModule,
    ComponentesModule
  ],
  declarations: [UsuarioProveedorPage]
})
export class UsuarioProveedorPageModule {}
