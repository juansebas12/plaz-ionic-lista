import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'inicio', pathMatch: 'full'
  },
  {
    path: 'inicio',
    loadChildren: () => import('./pages/inicio/inicio.module').then(m => m.InicioPageModule)
  },
  {
    path: 'ingresar',
    loadChildren: () => import('./pages/ingresar/ingresar.module').then(m => m.IngresarPageModule)
  },
  {
    path: 'proveedor/:nvPrv/:tipP',
    loadChildren: () => import('./pages/proveedor/proveedor.module').then(m => m.ProveedorPageModule)
  },
  {
    path: 'infopedprov/:idPed/:nvPriv/:tipP',
    loadChildren: () => import('./pages/pedido/infopedprov/infopedprov.module').then(m => m.InfopedprovPageModule)
  },
  {
    path: 'proveedores/:id',
    loadChildren: () => import('./pages/usuario-proveedor/usuario-proveedor.module').then(m => m.UsuarioProveedorPageModule)
  },
  {
    path: 'despacho/:id',
    loadChildren: () => import('./pages/despacho/despacho.module').then(m => m.DespachoPageModule)
  },
  {
    path: '**',
    loadChildren: () => import('./pages/inicio/inicio.module').then(m => m.InicioPageModule)
  },
  {
    path: 'despacho',
    loadChildren: () => import('./pages/despacho/despacho.module').then( m => m.DespachoPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
