import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ProveedorService {

  constructor(private http: HttpClient) { }
  url = "http://plaz2.agroni.co:5000";
  getProveedores() {
    return this.http.get(this.url+'/proveedor');
  }
  getProveedorProductos(id) {
    return this.http.get(this.url+'/proveedor/productos/'+id);
  }
  recibirProducto(peso,cantidad,id,idProducto,maduracion,presentacion,id_producto) {
    return this.http.get(this.url+'/proveedor/recibir?peso=' + peso + '&cantidad=' + cantidad + '&id='+id + '&idProducto=' + idProducto + '&maduracion=' + maduracion + '&presentacion=' + presentacion+ '&id_pro=' + id_producto);
  }
  UnidadesDeCompra(id) {
    return this.http.get(this.url+'/proveedor/presentacionesProducto/'+id);
  }
  maduraciones() {
    return this.http.get(this.url+'/proveedor/maduraciones');
  }
  usuariosOperaciones(){
    return this.http.get(this.url+'/pruebas/UsuariosOperacion');
  }
  dataOperaciones(id){
    return this.http.get(this.url+'/pruebas/historial/'+id);
  }
}
