import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DespachoService {

  constructor(private http: HttpClient) { }
  url = "http://localhost:5000";
  getPedidos(idProv: string) {
    return this.http.get(this.url+'/despacho');
  }
  cambiarEstado(id,estado){
    return this.http.get(this.url+'/despacho/cambiar?id='+id+'&estado='+estado);
  }
}


