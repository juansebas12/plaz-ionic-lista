import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class DataService {
  constructor(private http: HttpClient) { }
  
  url = "http://plaz2.agroni.co:5000";
  orm_path= "http://huerto.plaz.com.co:1111/";  
  orm_pathLocal = "http://localhost:1111/";
  getMensaje = this.orm_path + 'GetDataOrm/getMensajeCompra'

  getUsuario(prmUsu: string, prmCtr: string) {
    return this.http.get(this.url+'/user/login?prmUsu='  + prmUsu + '&prmCtr=' + prmCtr);
  }
  getUsuarioLogin() {
    return this.http.get(this.url+'/user/'+localStorage.getItem('id'));
  }
  getPedProveedor(idProv: string) {
    return this.http.get(this.url+'/pedidos');
  }
  getPedTransportadores() {
    return this.http.get(this.url+'/transportador');
  }
  getProveedores() {
    return this.http.get(this.url+'/Proveedores');
  }
  getInfoPedTrans(idPed: string) {
    return this.http.get(this.url+'/InfoXpedTrans?idPed=' + idPed);
  }
  getInfoPedProv(idPed: string, idProv: string){
    return this.http.get(this.url+'/pedidos/InfoXpedProv?idPed=' + idPed + '&idProv=' + idProv);
  }
  putEstadoPedido(idPed: string, compraitem: string,peso:string,id_usuario:string) {
    return this.http.get(this.url+'/pedidos/item?idPed=' + idPed + '&compraitem=' +compraitem + '&peso=' +peso+ '&id_usuario=' +id_usuario);
  }
  putEstadoProducto(idPed: string, idPrv: string, idProd: string, estPrm: number) {
    return this.http.get(this.url+'/ActualizaEstProducto?idPed=' + idPed + '&idPrv='
                        + idPrv + '&idProd=' + idProd + '&estPrm=' + estPrm);
  }
  finalizarPedido(idPed: string, idPrv: string) {
    return this.http.get(this.url+'/pedidos/finalizarPedido?idPed=' + idPed + '&idUser='+ idPrv) 
  }
  liberarPedido(idPed: string, idPrv: string) {
    return this.http.get(this.url+'/pedidos/LiberarPedido?idPed=' + idPed + '&idUser='+ idPrv) 
  }

  getMensajeCompra(idPed: string) {
    return new Promise((resolve, reject) => {
      let headers = new HttpHeaders();
      headers.append('Access-Control-Allow-Origin' , '*');
      headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
      headers.append('Accept', 'application/json');
      headers.append('content-type', 'application/json');
      let requestOptions =  { headers: headers };
      const datos = {
        "id" : idPed,        
      };
      this.http.post(this.getMensaje, datos, requestOptions)
      .subscribe(data => {
        resolve(data);
      }, error => {
        console.log(error);
        reject(error);
      });
    });
  }


}
