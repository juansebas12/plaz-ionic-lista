import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CIngresarComponent } from './c-ingresar/c-ingresar.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { CHeaderComponent } from './c-header/c-header.component';
import { CPedidosTranComponent } from './c-pedidos-tran/c-pedidos-tran.component';
import { CPedidosProvComponent } from './c-pedidos-prov/c-pedidos-prov.component';
import {CardSinAlistarComponent} from './usuario-proveedor/card-sin-alistar/card-sin-alistar.component'
//import {CardRechazadoComponent} from './usuario-proveedor/card-rechazado/card-rechazado.component'
//import {CardAlistadoComponent} from './usuario-proveedor/card-alistado/card-alistado.component'


@NgModule({
  declarations: [
    CIngresarComponent,
    CHeaderComponent,
    CPedidosTranComponent,
    CPedidosProvComponent,
    CardSinAlistarComponent,
    //CardRechazadoComponent,
    //CardAlistadoComponent
  ],
  exports: [
    CIngresarComponent,
    CHeaderComponent,
    CPedidosTranComponent,
    CPedidosProvComponent,
    CardSinAlistarComponent,
    //CardRechazadoComponent,
    //CardAlistadoComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ]
})
export class ComponentesModule { }
