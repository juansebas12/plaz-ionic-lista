import { DataService } from './../../servicios/data.service';
import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../servicios/alert.service';
import { NavController } from '@ionic/angular';



@Component({
  selector: 'app-c-ingresar',
  templateUrl: './c-ingresar.component.html',
  styleUrls: ['./c-ingresar.component.scss'],
})
export class CIngresarComponent implements OnInit {

  infoUsuario: any;
  tipUsu;
  usuario = {
    lgUsuario: null,
    lgContrasena: null
  };

  constructor(private alertCtrl: AlertService,
    private dataService: DataService,
    private navCtrl: NavController) { }

  ngOnInit() { 
  }

  async onIngresar() {
    this.dataService.getUsuario(this.usuario.lgUsuario, this.usuario.lgContrasena)
      .subscribe(async data => {
        console.log(data)
        localStorage.setItem('id',data['usuario']);
        localStorage.setItem('user',JSON.stringify(data));
        this.infoUsuario = data;
        if (data['error'] || data==null) {
          await this.alertCtrl.alertaError('Usuario incorrecto.');
        } else {
          this.redirigeUsuario(data['tipo_persona'], data['usuario']);
        }
      });
  }

  redirigeUsuario(tipUsu: string, priv: string) {
    switch (tipUsu) {
      case 'PROV':
        this.navCtrl.navigateRoot('/proveedores/'+priv);
        break;
      case 'ALIS':
        this.navCtrl.navigateRoot('/proveedor/' + priv + '/PROV');
        break;
      case 'DESP':
        this.navCtrl.navigateRoot('/despacho/'+priv);
        break;
    }
  }
}
