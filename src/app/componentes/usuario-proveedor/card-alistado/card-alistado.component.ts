import { Component, OnInit, Input  } from '@angular/core';
import { AlertController} from '@ionic/angular';
import { ProveedorService } from '../../../servicios/api/proveedor.service';

@Component({
  selector: 'app-card-alistado',
  templateUrl: './card-alistado.component.html',
  styleUrls: ['./card-alistado.component.scss'],
})
export class CardAlistadoComponent implements OnInit {

  constructor(public alertController: AlertController,private service: ProveedorService) { }
  @Input() producto: any;
  @Input() id_proveedor: any;
  ngOnInit() { }
  async recibir(idProducto) {
    const alert = await this.alertController.create({
      header: 'Ingrese al menos un dato',
      inputs: [
        {
          name: 'peso',
          type: 'number',
          placeholder: 'Peso (gr)',
        },
        {
          name: 'cantidad',
          type: 'number',
          placeholder: 'Cantidad',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if ((data.peso == "" || data.peso <= 0) && (data.cantidad == "" || data.cantidad <= 0)){
              return false;
            }
          }
        }
      ]
    });
    await alert.present();
  }
  async rechazar() {
    const alert = await this.alertController.create({
      header: 'Ingrese el motivo del rechazo',
      inputs: [
        {
          name: 'motivo',
          type: 'textarea',
          placeholder: 'motivo del rechazo',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if (data.motivo == ""){
              return false;
            }
            console.log(data);
          }
        }
      ]
    });
    await alert.present();
  }

}
