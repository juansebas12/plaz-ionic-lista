import { Component, OnInit, Input,Output, EventEmitter } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProveedorService } from '../../../servicios/api/proveedor.service';
import {RecibirPage} from '../../modals/proveedor/recibir/recibir.page'

@Component({
  selector: 'app-card-sin-alistar',
  templateUrl: './card-sin-alistar.component.html',
  styleUrls: ['./card-sin-alistar.component.scss'],
})
export class CardSinAlistarComponent implements OnInit {

  constructor(public modalController: ModalController,private service: ProveedorService) { }
  @Input() producto: any;
  @Input() id_proveedor: any;
  @Output() newCarga = new EventEmitter();

  ngOnInit() { }
  async recibir() {
    const modal = await this.modalController.create({
      component: RecibirPage,
      componentProps: {
        'producto': this.producto,
        'proveedor': this.id_proveedor,
      }
    });
    await modal.present();
    const {data} = await modal.onDidDismiss();
  }
  async rechazar() {
    console.log(this.producto);
    console.log(this.id_proveedor);
  }
}
