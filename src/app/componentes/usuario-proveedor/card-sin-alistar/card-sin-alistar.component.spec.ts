import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CardSinAlistarComponent } from './card-sin-alistar.component';

describe('CardSinAlistarComponent', () => {
  let component: CardSinAlistarComponent;
  let fixture: ComponentFixture<CardSinAlistarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardSinAlistarComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CardSinAlistarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
