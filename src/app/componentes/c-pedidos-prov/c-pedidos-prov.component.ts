import { DataService } from './../../servicios/data.service';
import { Component, OnInit, Input } from '@angular/core';
import { AlertController, NavController } from '@ionic/angular';
import { LoadingService } from '../../servicios/loading.service';
import { AlertService } from '../../servicios/alert.service';
import { async } from '@angular/core/testing';


@Component({
  selector: 'app-c-pedidos-prov',
  templateUrl: './c-pedidos-prov.component.html',
  styleUrls: ['./c-pedidos-prov.component.scss'],
})
export class CPedidosProvComponent implements OnInit {

  @Input() idPed: string;
  @Input() idProv: string;
  @Input() tipP: string;
  infoPedProv: any;
  mensajePedido: string;

  constructor(private dataService: DataService,
    private alertCtrl: AlertController,
    private alertCtrlS: AlertService,
    private loadingCtrl: LoadingService,
    private navCtrl: NavController,
    public alertController: AlertController) { }

  ngOnInit() {
    this.dataService.getMensajeCompra(this.idPed).then((data) => {      
      this.mensajePedido = data['data']['message'];      
    });
    this.dataService.getInfoPedProv(this.idPed, localStorage.getItem('id'))
      .subscribe((data) => {
        console.log(data)
        if (typeof(data) === 'string') {
          this.alertPedidoAsignado(data);
        } else {
          this.infoPedProv = data;
        }
      });
  }
  async alertPedidoAsignado(nombre){
    const alert = await this.alertController.create({
      header: 'Pedido Asignado',
      message: 'El PEDIDO ASIGNADO A : '+nombre,
      buttons: ['OK']
    });
    await alert.present();
    this.navCtrl.back();
  }
  finalizar(idPed){
    this.dataService.finalizarPedido(this.idPed, localStorage.getItem('id'))
      .subscribe((data) => {
        this.navCtrl.back();
      });
  }
  liberar(idPed){
    this.dataService.liberarPedido(this.idPed, localStorage.getItem('id'))
      .subscribe((data) => {
        this.navCtrl.back();
      });
  }
  async alistado(idPed, idcompraItem, peso) {
    this.dataService.putEstadoPedido(idPed, idcompraItem, peso,await localStorage.getItem('id'))
      .subscribe((data) => {
        this.infoPedProv = data;
        console.log(data);
      });
  }
  async peso(idPed, idcompraItem) {
    const alert = await this.alertController.create({
      header: 'Ingrese el peso (gr)',
      inputs: [
        {
          name: 'peso',
          type: 'number',
          placeholder: 'Peso',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Ok',
          handler: (data) => {
            if (data.peso == "" || data.peso <= 0) {
              return false;
            }
            this.alistado(idPed, idcompraItem, data.peso);
          }
        }
      ]
    });
    await alert.present();
  }
}
