import { DataService } from './../../servicios/data.service';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { AlertService } from 'src/app/servicios/alert.service';
import { AlertController, NavController, IonTextarea } from '@ionic/angular';
import { LoadingService } from '../../servicios/loading.service';

@Component({
  selector: 'app-c-pedidos-tran',
  templateUrl: './c-pedidos-tran.component.html',
  styleUrls: ['./c-pedidos-tran.component.scss'],
})
export class CPedidosTranComponent implements OnInit {

  @Input() idPed: string;
  @Input() tipP: string;
  @ViewChild('comentario', {static: false}) comentarios: IonTextarea;
  infoPedTrans: any;
  habilitado = false;

  constructor(private dataService: DataService,
              private alertCtrlS: AlertService,
              private alertCtrl: AlertController,
              private navCtrl: NavController,
              private loadingCtrl: LoadingService) { }

  ngOnInit() {
    this.dataService.getInfoPedTrans(this.idPed)
    .subscribe(data => {
        this.infoPedTrans = data;
        if (data[0].ped_estado === 'CONF'  || this.tipP === 'OPER') {
          this.habilitado = false;
        } else {
          this.habilitado = true;
        }
    });
  }

  onFinEntrg() {
    this.prAlertaConfirmacion();
  }

  async prAlertaConfirmacion() {
    const alert = await this.alertCtrl.create({
      header: '¡Confirmar Entrega!',
      message: '¿Desea confirmar la entrega del pedido <strong>' + this.idPed + '</strong>?',
      backdropDismiss: false,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Confirmar',
          handler: () => {
            this.loadingCtrl.prLoading('Confirmando Entrega.');
            this.dataService.putEstadoPedido(this.idPed, 'CONF', this.comentarios.value,'1')
            .subscribe(data => {
              this.loadingCtrl.closeLoading();
              // tslint:disable-next-line: no-string-literal
              if (data['affectedRows'] !== 0) {
                this.navCtrl.navigateForward('/transportadores/TRAN');
              } else {
                this.alertCtrlS.alertaError('No se ha podido actualizar la confirmacion del pedido.');
              }
            }, err => this.loadingCtrl.closeLoading() &&
                      this.alertCtrlS.alertaError('No se ha podido actualizar la confirmacion del pedido.')
            );
          }
        }
      ]
    });

    await alert.present();
  }
}
