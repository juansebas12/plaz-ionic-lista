import { Component, OnInit,Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProveedorService } from '../../../../servicios/api/proveedor.service';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-recibir',
  templateUrl: './recibir.page.html',
  styleUrls: ['./recibir.page.scss'],
})

export class RecibirPage implements OnInit {
  constructor(public modalController: ModalController,private service: ProveedorService,public alertController: AlertController) { }
  @Input() producto: any;
  @Input() proveedor: any;
  presentacion : any = '';
  maduracion : any = '';
  presentaciones : any;
  maduraciones : any;
  peso : any = '';
  cantidad : any = '';
  ngOnInit() {
    this.cargarPresentaciones();
    this.cargarMaduracion();
  }
  async apiRecibir(){
    this.service.recibirProducto(this.peso,this.cantidad,this.proveedor,this.producto.producto_id,this.maduracion,this.presentacion,this.producto.id_pro)
    .subscribe((data) => {
      if(data=='ok'){
        this.dismiss(data);
      }
    });
  }
  async dismiss(data) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Correcto',
      message: 'Producto Agregado Correctamente',
      buttons: ['OK']
    });
    await alert.present();
    this.modalController.dismiss({
      'dismissed': data
    });
  }
  cargarPresentaciones(){
    this.service.UnidadesDeCompra(this.producto.producto_id)
    .subscribe((data) => {
      console.log(data);
      this.presentaciones = data;
    });
  }
  cargarMaduracion(){
    this.service.maduraciones()
    .subscribe((data) => {
      console.log(data);
      this.maduraciones = data;
    });
  }
}
